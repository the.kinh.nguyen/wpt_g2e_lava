#!/bin/bash

source `pwd`/../U-GECKO_common/func_common.sh

source ${ENV_SET_SCRIPT}

TEST_NAME="Browser process existence check"

EXIT_STATUS=0
TEST_STATUS=0

if [ -e ${RESULTDIR} ]; then
	rm -r ${RESULTDIR}
fi

${TESTDIR}/test_start.sh "${TEST_NAME}"

#===============================================================================

#Browser Existence Check
BROWSER_EXISTENCE=0
if [ -e "${BROWSER_PID_FILE}" ]; then
	browserPID=`cat "${BROWSER_PID_FILE}"`

	if [ -e "/proc/${browserPID}/" ]; then
		BROWSER_EXISTENCE=1
	fi
fi

if [ ${BROWSER_EXISTENCE} = 0 ]; then
	echo "[test_log:`python ${TESTDIR}/hash.py Browser`:Browser process has been dead]"
	TEST_STATUS=1
fi

#===============================================================================

if [ ${TEST_STATUS} = 0 -a ${EXIT_STATUS} = 0 ]; then \
	echo "[test_result:OK]"
else
	echo "[test_result:NG]"
fi

${TESTDIR}/test_exit.sh "${TEST_NAME}"

exit ${EXIT_STATUS}
