#!/bin/bash

source `pwd`/../U-GECKO_common/func_common.sh

source ${ENV_SET_SCRIPT}

TEST_NAME="Browser control check"

EXIT_STATUS=0
TEST_STATUS=0

if [ -e ${RESULTDIR} ]; then
	rm -r ${RESULTDIR}
fi

${TESTDIR}/test_start.sh "${TEST_NAME}"

#===============================================================================

if ! python ./browser_control_check.py; then \
	echo "[test_log:`python ${TESTDIR}/hash.py Browser`:Cannot control via marionette]"
	TEST_STATUS=1
fi

#===============================================================================

if [ ${TEST_STATUS} = 0 -a ${EXIT_STATUS} = 0 ]; then \
	echo "[test_result:OK]"
else
	echo "[test_result:NG]"
fi

${TESTDIR}/test_exit.sh "${TEST_NAME}"

exit ${EXIT_STATUS}
