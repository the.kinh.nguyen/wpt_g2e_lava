#!/bin/bash

source `pwd`/../U-GECKO_common/func_common.sh

source ${ENV_SET_SCRIPT}

TEST_NAME="Browser executable check"

EXIT_STATUS=0
TEST_STATUS=0

if [ -e ${RESULTDIR} ]; then
	rm -r ${RESULTDIR}
fi

${TESTDIR}/test_start.sh "${TEST_NAME}"

#===============================================================================

#Find browser executable

#XXX:
#`which` command not being available,
#search each path directory by hand instead.
IFS=":"
EXECUTABLE_FOUND=0
for TARGET_PATH in ${PATH}; do \
	TARGET_PATH="${TARGET_PATH}/${BROWSER}"
	#echo ${TARGET_PATH}

	if [ -e ${TARGET_PATH} -a ! -d ${TARGET_PATH} -a -x ${TARGET_PATH} ]; then
		echo "Executable found: ${TARGET_PATH}"
		EXECUTABLE_FOUND=1
		break
	fi
done

if [ ${EXECUTABLE_FOUND} = 0 ]; then
	echo "[test_log:`python ${TESTDIR}/hash.py Browser`:Executable not found]"
	TEST_STATUS=1
fi

#===============================================================================

if ! ${BROWSER} --version; then
	echo "[test_log:`python ${TESTDIR}/hash.py Browser`:Version check failed]"
	TEST_STATUS=1
fi

#===============================================================================

if [ ${TEST_STATUS} = 0 -a ${EXIT_STATUS} = 0 ]; then \
	echo "[test_result:OK]"
else
	echo "[test_result:NG]"
fi

${TESTDIR}/test_exit.sh "${TEST_NAME}"

exit ${EXIT_STATUS}
