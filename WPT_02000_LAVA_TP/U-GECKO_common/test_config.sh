#!/bin/bash
# vim: ft=sh et ts=4 sts=4 sw=4

source `pwd`/../U-GECKO_common/func_common.sh

source ${ENV_SET_SCRIPT}

#===============================================================================

if [ -e ${TEST_COUNT_FILE} ]; then
    TEST_COUNT=`cat ${TEST_COUNT_FILE}`
else
    TEST_COUNT=0
fi

if [ -e ${BROWSER_PID_FILE} ]; then
    if [ ! -e /proc/`cat ${BROWSER_PID_FILE}`/ -o ${TEST_COUNT} -ge ${BROWSER_TEST_LIFE} ]; then
        #Browser process has been accidentally dead (some error might have happened),
        #or the test running count has reached its lifetime

        echo "Refresh environment..."

        #Terminate browser
        python ${TESTDIR}/terminate_browser.py
        if [ $? != 0 ]; then \
            echo "Browser terminate failed"
            exit 1;
        fi

        #${BROWSER_PID_FILE} is removed in the browser termination
    fi
fi

if [ ! -e ${BROWSER_PID_FILE} ]; then
    #Browser process does not exist (Env reset, or the very first execution)

    #Restart wayland if it is wayland env
    if systemctl status weston > /dev/null; then
        echo "Restarting wayland, it may take a few minutes..."

        #Reset env
        if [ ! -e ${DEFAULT_ENV_FILE} ]; then
            #Saved weston env does not exist
            #It should be saved in func_config.sh, whole test preparation
            echo "Weston env does not exist"
            exit 1
        fi

        t=${DEFAULT_ENV_FILE}
        #Current shell is bash, and the output format of `export` is `declare -x VAR="val"`
        for v in `export | awk '{print $3}' | awk -F= '{print $1}'`; do
            #${DEFAULT_ENV_FILE} or weston's environ file format is `VAR="val"`
            if ! (cat $t | awk '{print $1}' | awk -F= '{print $1}' | grep $v > /dev/null); then
                unset `echo $v`
            fi
        done

        #Restart weston
        pushd / > /dev/null
        echo weston restart
        /bin/bash -c "`echo \`cat $t\`` systemctl restart weston"
        if [ $? != 0 ]; then \
            echo "Weston restart failed"
            popd > /dev/null
            exit 1;
        else
            popd > /dev/null
        fi

        #Reload env
        source `pwd`/../U-GECKO_common/func_common.sh
        source ${ENV_SET_SCRIPT}
    fi

    #Launch browser
    echo "Launching the browser in test mode, it may take a few minutes..."

    #Copy Browser Profiles
    if [ -e ${TEMPDIR}/profiles ]; then \
        rm -r ${TEMPDIR}/profiles
    fi
    cp -r ${TESTDIR}/profiles ${TEMPDIR}/
    cp -r ${TEMPDIR}/cert_db/* ${TEMPDIR}/profiles/

    python ${TESTDIR}/launch_browser.py
    if [ $? != 0 ]; then \
        echo "Browser launch failed"
        exit 1;
    fi

    TEST_COUNT=0
fi

TEST_COUNT=`expr ${TEST_COUNT} + 1`
echo ${TEST_COUNT} > ${TEST_COUNT_FILE}

#===============================================================================

#Flush data
echo "Flushing the disk buffer, it may take a few minutes..."
sync;sync;sync

#===============================================================================

exit 0
