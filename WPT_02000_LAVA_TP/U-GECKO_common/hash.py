#!/usr/bin/env python
# vim: ft=python et ts=4 sts=4 sw=4

import sys
import hashlib

if(len(sys.argv) >= 2):
    srcStr  = sys.argv[1]
else:
    srcStr  = sys.stdin.read()

hashStr     = hashlib.sha1(srcStr).hexdigest()
print(hashStr)
