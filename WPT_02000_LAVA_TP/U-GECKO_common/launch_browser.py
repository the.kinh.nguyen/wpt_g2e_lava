#!/usr/bin/env python
# vim: ft=python et ts=4 sts=4 sw=4

import os
import sys
import signal
import subprocess

browserPID  = 0
err         = None

for retry in range(5):
    if retry > 0:
        print "retry"

    #-----------------------------------------------------------

    #Prepare browser process
    loggerProcess = subprocess.Popen([
                                        'sed', '-r',
                                        '-e', 's/#/*/g',
                                        '-e', 's/^/Browser: /'
                                     ],
                                     stdin=subprocess.PIPE)

    browserProcess = subprocess.Popen([
                            os.environ['BROWSER_cmd'], 'about:blank'
                        ],
                        stdout=loggerProcess.stdin,
                        stderr=loggerProcess.stdin,
			shell=True)

    browserPID = browserProcess.pid

    #-----------------------------------------------------------

    #Other settings
    f = open(os.environ['BROWSER_PID_FILE'], 'w')
    f.write(str(browserPID))
    f.close()

    f = open(os.environ['TEST_COUNT_FILE'], 'w')
    f.write(str(0))
    f.close()

    try:
        from marionette_driver import marionette
        client = marionette.Marionette('localhost', port=2828)
        client.start_session()

        #.......................................................

        #XXX:
        #Now the browser is likely to crash and come to be too small window size,
        #so resize it here.
        client.maximize_window()

        #.......................................................

        err = None
        break
    except:
        #.......................................................

        err = sys.exc_info()
        print err

        if os.path.exists('/proc/' + str(browserPID) + '/'):
            os.kill(browserPID, signal.SIGINT)
        os.remove(os.environ['BROWSER_PID_FILE'])

        continue

if not os.path.exists(os.environ['BROWSER_PID_FILE']) or not os.path.exists('/proc/' + str(browserPID) + '/'):
    if err != None:
        raise err
