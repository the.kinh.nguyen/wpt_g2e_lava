#!/usr/bin/env python
# vim: ft=python et ts=4 sts=4 sw=4

from marionette_driver import marionette
client = marionette.Marionette('localhost', port=2828)
client.start_session()

for handle in reversed(client.window_handles):
    client.switch_to_window(handle)
    client.close()
    client.start_session()

client.navigate('about:blank')
