#!/bin/bash
# vim: ft=sh et ts=4 sts=4 sw=4

source `pwd`/../U-GECKO_common/func_common.sh

source ${ENV_SET_SCRIPT}

#===============================================================================

TEST_NAME=$1

echo ${TEST_NAME} > ${CURRENT_TEST_NAME_FILE}

echo "[test_start:${TEST_NAME}]"

${TESTDIR}/test_timeout_check.sh "${TEST_NAME}" &

exit 0
