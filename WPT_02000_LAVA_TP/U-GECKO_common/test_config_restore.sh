#!/bin/bash
# vim: ft=sh et ts=4 sts=4 sw=4

source `pwd`/../U-GECKO_common/func_common.sh

source ${ENV_SET_SCRIPT}

#===============================================================================

python ${TESTDIR}/close_browser_tabs.py

if [ $? = 0 ]; then \
    exit 0;
else
    exit 1;
fi
