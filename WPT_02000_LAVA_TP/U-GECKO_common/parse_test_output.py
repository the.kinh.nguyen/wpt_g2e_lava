#!/usr/bin/env python
# vim: ft=python et ts=4 sts=4 sw=4

import json
import re
import os
import sys
import hashlib

WPTRUNNERDIR        = os.path.join(os.environ["TESTDIR"], "wptrunner")
WPTDIR              = os.path.join(os.environ["TESTDIR"], "web-platform-tests")
sys.path.append(WPTRUNNERDIR)

from wptrunner      import manifestupdate

metadata_root       = os.path.join(os.environ["TESTDIR"], "wptrunner-metadata")
url_base            = WPTDIR
test_path           = None
manifest            = None

#{
#   "status":       "FAIL",
#   "thread":       "Thread-TestrunnerManager-1",
#   "source":       "web-platform-tests",
#   "pid":          618,
#   "stack":        "isNukedFromNode/<@http://web-platform.test:8000/dom/historical.html:127:5\nTest.prototype.step@http://web-platform.test:8000/resources/testharness.js:1409:20\ntest@http://web-platform.test:8000/resources/testharness.js:501:9\nisNukedFromNode@http://web-platform.test:8000/dom/historical.html:123:1\n@http://web-platform.test:8000/dom/historical.html:144:1\n",
#   "subtest":      "Node member must be nuked: localName",
#   "time":         1487281854282,
#   "test":         "/dom/historical.html",
#   "message":      "assert_equals: expected (undefined) undefined but got (object) null",
#   "action":       "test_status",
#   "expected":     "PASS"
#}

testFail        = False
logPath         = sys.argv[1]
resultFp        = None

LOG_MAX_LENGTH  = 400

try:
    resultFp    = open(logPath, 'r')

    dumpKeyList = ["subtest", "message"]
    for l in resultFp:
        jl = json.loads(l)

        #Read additional manifest
        if jl.has_key("test") and test_path != jl["test"]:
            test_path   = jl["test"]
            manifest    = manifestupdate.get_manifest(metadata_root, test_path, url_base)
            if manifest != None:
                while not (hasattr(manifest, "_from_file") and manifest._from_file):
                    assert(hasattr(manifest, "children"))
                    assert(len(manifest.children) == 1)
                    manifest        = manifest.children[0]

        if jl.has_key("status"):
            expected    = None
            if jl.has_key("subtest") and manifest != None and hasattr(manifest, "subtests") and manifest.subtests.has_key(jl["subtest"]):
                expected    = manifest.subtests[jl["subtest"]]

                assert(hasattr(expected, "_data"))
                expected    = expected._data

            subTestFail         = False
            #print "%s" % (jl["subtest"] if jl.has_key("subtest") else "---")
            if expected == None:
                subTestFail     = jl["status"] == "FAIL" or jl["status"] == "ERROR"
                #print "\t %s <=> %s" % (jl["status"], "PASS / OK")
            else:
                assert(expected.has_key("expected"))
                for e in expected["expected"]:
                    assert(hasattr(e, "value_node"))
                    e = e.value_node
                    assert(hasattr(e, "data"))

                    #print "\t %s <=> %s" % (jl["status"], e.data)
                    if jl["status"] != e.data:
                        subTestFail     = True
                        break

            if subTestFail:
                testFail = True
                keyword = hashlib.sha1((jl["subtest"]) if (jl.has_key("subtest")) else ("Test")).hexdigest()
                for key in dumpKeyList:
                    if jl.has_key(key):
                        comment = re.sub(r'([\[\]\\])',
                                         r'\\\1',
                                         jl[key][:LOG_MAX_LENGTH])
                        print("[test_log:%s:<%s> %s]" % (keyword, key, comment))
except:
    print("[test_log:%s:Output file parse failed: %s]" % (hashlib.sha1("Test").hexdigest(), logPath))
    testFail = True

if resultFp != None:
    resultFp.close()

if testFail:
    sys.exit(1)
else:
    sys.exit(0)
