#!/usr/bin/env python
# vim: ft=python et ts=4 sts=4 sw=4

import os
import signal

if os.path.exists(os.environ['BROWSER_PID_FILE']):
    f = open(os.environ['BROWSER_PID_FILE'], 'r')
    browserPID = int(f.read())
    f.close()

    if os.path.exists('/proc/' + str(browserPID) + '/'):
        os.kill(browserPID, signal.SIGINT)

        #XXX:
        #os.waitpid(browserPID, 0)
        #raises "OSError: [Errno 10] No child processes"
        #Instead, check the proc directory
        import time
        while os.path.exists('/proc/' + str(browserPID) + '/'):
            time.sleep(1)

    os.remove(os.environ['BROWSER_PID_FILE'])

if os.path.exists(os.environ['TEST_COUNT_FILE']):
    os.remove(os.environ['TEST_COUNT_FILE'])
