#!/bin/bash
# vim: ft=sh et ts=4 sts=4 sw=4

source `pwd`/../U-GECKO_common/func_common.sh

#===============================================================================

#Prepare Directories
#if [ -e ${TEMPDIR} -o -e ${BACKUPDIR} -o -e /etc/hosts.bak ]; then
if [ -e ${TEMPDIR} -o -e /etc/hosts.bak ]; then
    ${TESTDIR}/func_config_restore.sh
fi

#if [ -e ${BACKUPDIR} ]; then
#   rm -r ${BACKUPDIR}
#fi

if [ -e ${TEMPDIR} ]; then
    rm -r ${TEMPDIR}
fi

#mkdir -p ${BACKUPDIR}
mkdir -p ${TEMPDIR}

#===============================================================================

#Set Environment
echo '#!/bin/bash'                                                              >  ${ENV_SET_SCRIPT}
echo ''                                                                         >> ${ENV_SET_SCRIPT}
echo 'export LD_LIBRARY_PATH="${TESTDIR}/modules/usr/lib64:${TESTDIR}/modules/usr/lib:${LD_LIBRARY_PATH}"' \
                                                                                >> ${ENV_SET_SCRIPT}
echo 'export PATH="${TESTDIR}/modules/usr/bin:${PATH}"'                         >> ${ENV_SET_SCRIPT}

if [ -e "${TESTDIR}/web-platform-tests/tools" ]; then
    cd ${TESTDIR}/web-platform-tests/tools

    echo ''                                                                     >> ${ENV_SET_SCRIPT}

    #XXX:   Some of Python libs in web-platform-tests conflict
    #       with the libs in U-GECKO_common.
    #       Avoid them to add to `${PYTHONPATH}`
    for p in *; do \
        if [ \
            -d ${p} -a -f ${p}/setup.py \
            -a ! -e ${TESTDIR}/modules/usr/lib/python2.7/site-packages/${p}-*.dist-info/METADATA \
        ]; then \
            echo "export PYTHONPATH=\"\${PYTHONPATH}:\${TESTDIR}/web-platform-tests/tools/${p}\""                   >> ${ENV_SET_SCRIPT}
        fi
    done

    cd -
fi

chmod a+x ${ENV_SET_SCRIPT}

source ${ENV_SET_SCRIPT}

#===============================================================================

#Save weston env
if [ -e /etc/init.d/weston ]; then
    westonPID=`python ./pidof.py weston`
    if [ "${westonPID}" = "" ]; then
        echo "Weston is dead"
        exit 1
    fi

    cat /proc/${westonPID}/environ \
        | python3 -c 'import sys, re; s = sys.stdin.read(); s=re.sub("\x00", "\n", s); print(s);' \
        > ${DEFAULT_ENV_FILE}
fi

#===============================================================================

#Copy /etc/hosts
mv /etc/hosts /etc/hosts.bak
cp ${TESTDIR}/etc/hosts /etc/hosts

#===============================================================================

#Copy fonts
if [ -d "${FONT_DST_DIR}" ]; then
    FONT_CREATE_DIR=""
else
    FONT_CREATE_DIR="$(mkdir -pv "${FONT_DST_DIR}" | sed -E "s/^.*'(.*)'$/\1/" | sed -n -e '1!G;h;$p')"
fi

FONT_CREATE_FILE=""
for f in "${FONT_SRC_DIR}"/*.* "${FONT_SRC_DIR}"/**/*.*; do \
    if [ ! -f "${FONT_DST_DIR}/$(basename "${f}")" ]; then
        cp "${f}" "${FONT_DST_DIR}/"
        FONT_CREATE_FILE+=" $(basename "${f}")"
    fi
done

echo ""                                                                         >> ${ENV_SET_SCRIPT}
echo "export FONT_CREATE_DIR=\"${FONT_CREATE_DIR}\""                            >> ${ENV_SET_SCRIPT}
echo "export FONT_CREATE_FILE=\"${FONT_CREATE_FILE}\""                          >> ${ENV_SET_SCRIPT}

fc-cache -f

#===============================================================================

#Prepare SSL Certificate
if ! python ./prepare_ssl_cert.py; then \
    echo "SSL certification prepare failed"
    exit 1
fi

#===============================================================================

#Flush data
echo "Flushing the disk buffer, it may take a few minutes..."
sync;sync;sync

#===============================================================================

exit 0
