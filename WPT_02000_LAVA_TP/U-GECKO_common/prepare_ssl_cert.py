#!/usr/bin/env python
# vim: ft=python et ts=4 sts=4 sw=4

import os
import sys
import subprocess
import shutil

WPTDIR  = os.path.join(os.environ["TESTDIR"], "web-platform-tests")
sys.path.append(WPTDIR)

from tools.serve    import serve

#===============================================================================

#def get_ssl_environment():
#    config  = serve.build_config(
#                os.path.join(WPTDIR, "config.json")
#            )
#
#    serve.setup_logger(config["log_level"])
#
#    implementation_type = config["ssl"]["type"]
#    if("base_path" in config["ssl"][implementation_type]):
#        config["ssl"][implementation_type]["base_path"]     = os.path.join(os.environ["TEMPDIR"], config["ssl"][implementation_type]["base_path"])
#
#    with serve.get_ssl_environment(config) as ssl_env:
#        host            = config["host"]
#        domains         = serve.get_subdomains(host)
#        ports           = serve.get_ports(config, ssl_env)
#        bind_hostname   = config["bind_hostname"]
#
#        paths           = {
#            "doc_root":     config["doc_root"],
#            "ws_doc_root":  config["ws_doc_root"]
#        }
#
#        external_config = serve.normalise_config(config, ports)
#
#        if(not "domains" in config):
#            config["domains"]   = external_config["domains"] if "domains" in external_config else {"0": config["host"]}
#        ssl_config      = serve.get_ssl_config(config, ssl_env)
#
#        ssl_config["type"]          = implementation_type
#        if("base_path" in config["ssl"][implementation_type]):
#            ssl_config["base_path"]     = config["ssl"][implementation_type]["base_path"]
#
#        if(hasattr(ssl_env, "_ca_cert_path") and ssl_env._ca_cert_path!=None):
#            ssl_config["ca_cert_path"]  = ssl_env._ca_cert_path
#        else:
#            if("host_cert_path" in config["ssl"][implementation_type]):
#                ca_cert_default_path        = os.path.join(WPTDIR
#                                            ,   os.path.dirname(config["ssl"][implementation_type]["host_cert_path"])
#                                            ,   "cacert.pem"
#                                            )
#            else:
#                ca_cert_default_path        = os.path.join(config["ssl"][implementation_type]["base_path"]
#                                            ,   "cacert.pem"
#                                            )
#            if(os.path.exists(ca_cert_default_path)):
#                ssl_config["ca_cert_path"]  = ca_cert_default_path
#
#        if(hasattr(ssl_env, "_ca_key_path") and ssl_env._ca_key_path!=None):
#            ssl_config["ca_key_path"]   = ssl_env._ca_key_path
#        else:
#            if("host_key_path" in config["ssl"][implementation_type]):
#                ca_key_default_path         = os.path.join(WPTDIR
#                                            ,   os.path.dirname(config["ssl"][implementation_type]["host_key_path"])
#                                            ,   "cakey.pem"
#                                            )
#            else:
#                ca_key_default_path         = os.path.join(config["ssl"][implementation_type]["base_path"]
#                                            ,   "cakey.pem"
#                                            )
#            if(os.path.exists(ca_key_default_path)):
#                ssl_config["ca_key_path"]   = ca_key_default_path
#
#    return ssl_config

#===============================================================================

def resolveKeyPath(keyPath):
    if(os.path.exists(keyPath)):
        return keyPath

    if(os.path.exists(os.path.join(WPTDIR, keyPath))):
        return os.path.join(WPTDIR, keyPath)

    return keyPath

#===============================================================================

#ssl_config  = get_ssl_environment()

config  = serve.build_config(os.path.join(WPTDIR, "config.json"))


f = open(os.environ["SSL_KEY_SETTING"], "w")

#import json
#json.dump(ssl_config, f)
#f.write("\n\n")

f.write("#!/bin/bash\n\n")

f.write("export SSL_CA_CERT_PATH=%s\n"      % resolveKeyPath(os.path.join(WPTDIR,os.path.dirname(config._default["ssl"]["pregenerated"]["host_cert_path"]),"cacert.pem")))

f.write("export SSL_HOST_CERT_PATH=%s\n"    % resolveKeyPath(config._default["ssl"]["pregenerated"]["host_cert_path"]))

f.write("export SSL_HOST_KEY_PATH=%s\n"     % resolveKeyPath(config._default["ssl"]["pregenerated"]["host_key_path"]))

f.close()

os.chmod(os.environ["SSL_KEY_SETTING"], 0755)

#===============================================================================

#Set GECKO profile
def certutil(*args):
    print " ".join([CERTUTIL_BIN] + list(args))
    subprocess.call(" ".join([CERTUTIL_BIN] + list(args)), shell=True)

CERTUTIL_BIN    = os.path.join(os.environ["TESTDIR"], "modules/usr/bin/certutil")
cert_db_path    = os.path.join(os.environ["TEMPDIR"], "cert_db")
pw_path         = os.path.join(cert_db_path, ".crtdbpw")

if(os.path.exists(cert_db_path)):
    shutil.rmtree(cert_db_path)

os.mkdir(cert_db_path);

with open(pw_path, "w") as f:
    # Use empty password for certificate db
    f.write("\n")

# Create a new certificate db
certutil("-N", "-d", cert_db_path, "-f", pw_path)

# Add the CA certificate to the database and mark as trusted to issue server certs
certutil("-A", "-d", cert_db_path, "-f", pw_path, "-t", "CT,,",
         "-n", "web-platform-tests", "-i", os.path.join(WPTDIR,os.path.dirname(config._default["ssl"]["pregenerated"]["host_cert_path"]),"cacert.pem"))

# List all certs in the database
certutil("-L", "-d", cert_db_path)
