#!/bin/bash
# vim: ft=sh et ts=4 sts=4 sw=4

source `pwd`/../U-GECKO_common/func_common.sh

source ${ENV_SET_SCRIPT}

#===============================================================================

TEST_NAME=$1

if [ -e ${CURRENT_TEST_NAME_FILE} -a "`cat ${CURRENT_TEST_NAME_FILE}`" = "${TEST_NAME}" ]; then \
    rm ${CURRENT_TEST_NAME_FILE}
fi

echo "[test_exit]"

exit 0
