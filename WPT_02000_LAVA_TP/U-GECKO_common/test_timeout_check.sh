#!/bin/bash
# vim: ft=sh et ts=4 sts=4 sw=4

source `pwd`/../U-GECKO_common/func_common.sh

source ${ENV_SET_SCRIPT}

#===============================================================================

TEST_NAME=$1
LOOP_INTERVAL=1
RUNNING_TIME=0

while true; do \
    sleep ${LOOP_INTERVAL}
    RUNNING_TIME=`expr ${RUNNING_TIME} + ${LOOP_INTERVAL}`

    if [ ! -e ${CURRENT_TEST_NAME_FILE} ]; then \
        break
    fi

    if [ "`cat ${CURRENT_TEST_NAME_FILE}`" != "${TEST_NAME}" ]; then \
        break
    fi

    if [ ${RUNNING_TIME} -ge ${TEST_TIMEOUT_MAX_RUNNING_TIME} ]; then \
        echo "Test running time has reached the limit: ${RUNNING_TIME} / ${TEST_TIMEOUT_MAX_RUNNING_TIME}"
        echo "No timeout extension any more"
        break
    fi

    #echo "Running Time: ${RUNNING_TIME}"
    if [ `expr ${RUNNING_TIME} % ${TEST_TIMEOUT_CHECK_INTERVAL}` -eq 0 ]; then \
        echo "[test_timeout:${TEST_TIMEOUT_OVERTIME}]"
    fi
done

exit 0
