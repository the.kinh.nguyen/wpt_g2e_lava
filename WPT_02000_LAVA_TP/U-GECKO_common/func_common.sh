#!/bin/bash
# vim: ft=sh et ts=4 sts=4 sw=4

source `pwd`/../U-GECKO_common/browser-config.sh

export TEST_NAME="U-GECKO"
export WORKDIR=`pwd`
export TESTDIR="${WORKDIR}/../${TEST_NAME}_common"
export TEMPDIR="/var/tmp/${TEST_NAME}_Test"
export SSL_KEY_SETTING="${TEMPDIR}/sslKeySetting.sh"
export RESULTDIR="${WORKDIR}/result"
#export BACKUPDIR="${TESTDIR}/BACKUP"
export ENV_SET_SCRIPT="${TEMPDIR}/envSet.sh"
export BROWSER_PID_FILE="${TEMPDIR}/browserPID.txt"
export TEST_COUNT_FILE="${TEMPDIR}/testCount.txt"
export BROWSER_TEST_LIFE=50
export DEFAULT_ENV_FILE="${TEMPDIR}/defaultWestonEnv.txt"
export CURRENT_TEST_NAME_FILE="${TEMPDIR}/currentTest.txt"
export TEST_TIMEOUT_CHECK_INTERVAL=60
export TEST_TIMEOUT_OVERTIME=80
export TEST_TIMEOUT_MAX_RUNNING_TIME=300
export FONT_DST_DIR="$(ls -d ~)/.local/share/fonts"
export FONT_SRC_DIR="${TESTDIR}/web-platform-tests/fonts"
