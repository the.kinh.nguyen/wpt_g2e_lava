#!/bin/bash
# vim: ft=sh et ts=4 sts=4 sw=4

source `pwd`/../U-GECKO_common/func_common.sh

#===============================================================================

#Process Temp Directory
if [ -e ${TEMPDIR} ]; then
    #Terminate Browser
    source ${ENV_SET_SCRIPT}
    python ./terminate_browser.py

    rm -r ${TEMPDIR}
fi

#===============================================================================

#Remove fonts
for f in ${FONT_CREATE_FILE}; do \
    rm "${FONT_DST_DIR}/${f}"
done

for d in ${FONT_CREATE_DIR}; do \
    rmdir "${d}"
done

fc-cache -f

#===============================================================================

#Process Backup Directory
if [ -e /etc/hosts.bak ]; then
    mv /etc/hosts.bak /etc/hosts
fi

#if [ -e ${BACKUPDIR} ]; then
#   rm -r ${BACKUPDIR}
#fi

#===============================================================================

exit 0
