#!/usr/bin/env python

import os
import sys

if len(sys.argv) != 2:
    sys.exit('Usage: %s program' % sys.argv[0])

pids = []

for dir in os.listdir('/proc'):
    if not dir.isdigit():
        continue

    path = os.path.join('/proc', dir, 'comm')
    try:
        with open(path) as f:
            command = f.readline().strip()
    except:
        continue

    if command == sys.argv[1]:
        pids.append(dir)

if len(pids) == 0:
    sys.exit(1)

print ' '.join(pids)
