#!/bin/bash
# vim: ft=sh et ts=4 sts=4 sw=4

source `pwd`/../U-GECKO_common/func_common.sh

source ${ENV_SET_SCRIPT}

OUTPUT="${RESULTDIR}/output.log"
TEST_OPTION=""
TEST_OPTION+="--no-pause-after-test"
TEST_OPTION+=" --use-existing-gecko"
TEST_OPTION+=" --log-mach=-"
TEST_OPTION+=" --log-raw=${OUTPUT}"

if [ -e ${SSL_KEY_SETTING} ]; then
    source ${SSL_KEY_SETTING}

    if [ ! -z "${SSL_CA_CERT_PATH}" ]; then
        TEST_OPTION+=" --ca-cert-path=${SSL_CA_CERT_PATH}"
    fi

    if [ ! -z "${SSL_HOST_CERT_PATH}" ]; then
        TEST_OPTION+=" --host-cert-path=${SSL_HOST_CERT_PATH}"
    fi

    if [ ! -z "${SSL_HOST_KEY_PATH}" ]; then
        TEST_OPTION+=" --host-key-path=${SSL_HOST_KEY_PATH}"
    fi
fi

TEST_NAME=$1
shift 1

EXIT_STATUS=0
TEST_STATUS=0

if [ -e ${RESULTDIR} ]; then
    rm -r ${RESULTDIR}
fi

mkdir -p ${RESULTDIR}

${TESTDIR}/test_start.sh "${TEST_NAME}"

#===============================================================================

#Browser Existence Check
BROWSER_EXISTENCE=0
if [ -e "${BROWSER_PID_FILE}" ]; then
    browserPID=`cat "${BROWSER_PID_FILE}"`

    if [ -e "/proc/${browserPID}/" ]; then
        BROWSER_EXISTENCE=1
    fi
fi

if [ ${BROWSER_EXISTENCE} = 0 ]; then
    echo "[test_log:`python ${TESTDIR}/hash.py Browser`:Browser process has been dead]"
    EXIT_STATUS=1
fi

#===============================================================================

#Execute Test
if [ ! ${BROWSER_EXISTENCE} = 0 ]; then
    python ${TESTDIR}/wptrunner/runtest.py \
        --metadata=${TESTDIR}/wptrunner-metadata \
        --tests=${TESTDIR}/web-platform-tests \
        --binary=/usr/bin/${BROWSER} \
        --certutil-binary=${TESTDIR}/modules/usr/bin/certutil \
        --prefs-root=${TEMPDIR}/profiles \
        --config=${TESTDIR}/wptrunner/wptrunner.default.ini \
        --run-info=${TESTDIR}/wptrunner-metadata/mozinfo.json \
        ${TEST_OPTION} \
        $@
    TEST_STATUS=$?

    if [ ! ${TEST_STATUS} = 0 ]; then
        echo "[test_log:`python ${TESTDIR}/hash.py Test`:Python exit status bad, status=${TEST_STATUS}]"

        #XXX:
        #Do not set test_exec.sh's exit status here, by python's exit status.
        #EXIT_STATUS=1
    fi
fi

#-------------------------------------------------------------------------------

#Check the Test Result
if [ -e ${OUTPUT} -a ! ${TEST_STATUS} = 0 ]; then
    python ${TESTDIR}/parse_test_output.py ${OUTPUT}
fi

#===============================================================================

if [ ${TEST_STATUS} = 0 -a ${EXIT_STATUS} = 0 ]; then \
    echo "[test_result:OK]"
else
    echo "[test_result:NG]"
fi

${TESTDIR}/test_exit.sh "${TEST_NAME}"

exit ${EXIT_STATUS}
