// Empty base preferences file used by everything.
// This is useful for testing a pref on try.
/* globals user_pref */
user_pref("marionette.enabled", true);
user_pref("marionette.debugging.clicktostart", false);
user_pref("marionette.log.level", "Trace");
user_pref("marionette.log.truncate", true);
user_pref("marionette.port", 2828);
user_pref("marionette.prefs.recommended", true);
user_pref("marionette.contentListener", false);
user_pref("dom.disable_open_during_load", false);
user_pref("network.dns.localDomains", "web-platform.test,www.web-platform.test,www1.web-platform.test,www2.web-platform.test,xn--n8j6ds53lwwkrqhv28a.web-platform.test,xn--lve-6lad.web-platform.test");
user_pref("network.proxy.type", 0);
user_pref("places.history.enabled", false);

user_pref("browser.shell.checkDefaultBrowser", false);

user_pref("reftest.wait.milliseconds", 1000);

//#user_pref("font.name-list.serif.x-western", "Sazanami Mincho");
//#user_pref("font.name-list.sans-serif.x-western", "Sazanami Gothic");
//#user_pref("font.name-list.monospace.x-western", "Sazanami Gothic");
//#user_pref("font.name-list.cursive.x-western", "Sazanami Gothic");
