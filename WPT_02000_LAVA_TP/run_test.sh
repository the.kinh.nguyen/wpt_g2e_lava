#!/bin/bash
# vim: ft=sh et ts=4 sts=4 sw=4

WORKDIR=`pwd`

#===============================================================================

pushd ${WORKDIR}/U-GECKO_common > /dev/null
    ./func_config.sh
    status=$?
popd > /dev/null

if [ ${status} != 0 ]; then \
    echo "Initialization failed: status=${status}"
fi

#===============================================================================

if [ ${status} = 0 ]; then \
    for TEST in ${WORKDIR}/U-GECKO_????_????_????_????; do
        pushd ${TEST} > /dev/null
            if [ -x ./test_config.sh ]; then
                ./test_config.sh
            fi

            ./test_exec.sh

            if [ -x ./test_config_restore.sh ]; then
                ./test_config_restore.sh
            fi
        popd > /dev/null
    done
fi

#===============================================================================

pushd ${WORKDIR}/U-GECKO_common > /dev/null
    ./func_config_restore.sh
popd > /dev/null

#===============================================================================

exit 0
